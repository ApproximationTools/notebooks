import numpy as np
import scipy as sp
import matplotlib
matplotlib.use('Agg')

import sys
import os
HILBERTPATH = os.path.join(os.path.dirname(__file__), '../../..')
sys.path.append(HILBERTPATH)
import pyhilbert as hil
import pyredmod as rm
import pdb

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(points):
        field = hil.Vector((a_bar + c * a_bar + c * p.reshape((2**field_div,2**field_div))).flatten(), space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

Wm_type = sys.argv[1]
m_or_div = int(sys.argv[2])

if Wm_type not in ['reg', 'rand'] or len(sys.argv) != 3:
    print(f'Error - call with arguments: {sys.argv[0]} [rand|reg] m')
    sys.exit()

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)

field_div = 2
L2_coarse = hil.L2DyadicSq(field_div)

a_bar = 1.0
c = 0.9
side_n = 2**field_div

np.random.seed(3)
#
# Make the training set and the test set of solutions on the manifold
#
N_test_set = int(5e2)
test_params = 2*np.random.random((N_test_set, side_n*side_n)) - 1
test_set, test_fields = make_soln(test_params, L2_coarse, H1_fine, a_bar=a_bar, c=c, f=1.0)

N_train_set = int(5e3)
train_params = 2 * np.random.random((N_train_set, side_n * side_n)) - 1
train_set, train_fields = make_soln(train_params, L2_coarse, H1_fine, a_bar, c, f=1.0)

#
# Make the two measurement spaces of regularly and randomly placed local averages...
# local_width is the width of the measurement squares in terms of FEM mesh squares
# ie. 1 means 2^1 = width of 2
#
width_div = 1

if Wm_type == 'reg':
    if m_or_div > fem_div:
        print('Meas. div too large for FEM grid')
        sys.exit()
    Wm = rm.make_local_avg_grid_basis(width_div, m_or_div, fem_div).orthonormalise()
    m = Wm.n
if Wm_type == 'rand':
    m = m_or_div
    Wm = rm.make_local_avg_random_basis(m, fem_div, width=2**width_div).orthonormalise()

#
# Make sinusoid approx basis
#
Vn_sin = rm.make_sin_basis(div=fem_div, N=m)

#
# Make random snapshot basis
#
Vn_red_params = 2 * np.random.random((m, side_n * side_n)) - 1
Vn_red, Vn_red_fields = make_soln(Vn_red_params, L2_coarse, H1_fine, a_bar, c, f=1.0)

#
# Make PCA basis / estimator
#
Vn_PCA, sigma_sq, center = rm.pca_from_samples(train_set)

train_set_cent = train_set - center

Vns_linear = [Vn_sin, Vn_red, Vn_PCA]
Vns_affine = [Vn_sin, Vn_red, Vn_PCA]

#
# Make greedily chosen reduced basis
#
g = rm.GreedyApprox(train_set, verbose=True)
g.construct_to_n(m)
Vns_linear.append(g.Vn[:m])

g = rm.GreedyApprox(train_set_cent, verbose=True)
g.construct_to_n(m)
Vns_affine.append(g.Vn[:m])

# Now do the reconstruction and error rates here
 # 4 stats, linear/affine, 6 Vns (sin, red, greedy, PCA, omp, pp), 8 Wms
stats = np.zeros([4, 2, 6, N_test_set, m])
    
# Measure the test_set here:
ws = Wm.A @ test_set

for j, g in enumerate(Vns_linear):
    Vn_big = g.orthonormalise()
    print(f'Linear alg {j}') 
    for l, n in enumerate(range(2,min(Vn_big.n, m))):
        print(f'n={n}, u: ', end='')
        Vn = Vn_big[:n]
        lin_est = rm.LinearWorstCaseEstimator(Vn=Vn, Wm=Wm)
        u_stars, v_stars = lin_est.best_estimator(ws)
        u_p_vs = Vn @ (Vn.A @ test_set)

        stats[2, 0, j, :, n] = lin_est.beta()
        stats[3, 0, j, :, n] = lin_est.cond
        for k, u in enumerate(test_set):                 
            print(f'{k}, ', end='')
            stats[0, 0, j, k, n] = (u - u_stars[k]).norm()
            stats[1, 0, j, k, n] = (u - u_p_vs[k]).norm()
        print('')

for j, g in enumerate(Vns_affine):
    Vn_big = g.orthonormalise()
    print(f'affine alg {j}')
    for l, n in enumerate(range(2,min(Vn_big.n, m))):
        print(f'n={n}, u: ')
        Vn = Vn_big[:n]
        lin_est = rm.LinearWorstCaseEstimator(Vn=Vn, center=center, Wm=Wm)
        u_stars, v_stars = lin_est.best_estimator(ws)
        u_p_vs = Vn @ (Vn.A @ (test_set - center))

        stats[2, 1, j, :, n] = lin_est.beta()
        stats[3, 1, j, :, n] = lin_est.cond
        for k, u in enumerate(test_set):                
            print(f'{k}, ', end='')
            stats[0, 1, j, k, n] = (u - u_stars[k]).norm()
            stats[1, 1, j, k, n] = (u - center - u_p_vs[k]).norm()
        print('')

print(f'Calculating Wm-adaptive approximations')
algs_linear = [rm.MeasBasedOMP(train_set, test_set[0], Wm, verbose=True), 
               rm.MeasBasedPP(train_set, test_set[0], Wm, verbose=True)]

algs_affine = [rm.MeasBasedOMP(train_set_cent, test_set[0] - center, Wm, verbose=True), 
               rm.MeasBasedPP(train_set_cent, test_set[0] - center, Wm, verbose=True)]

for j, alg in enumerate(algs_linear):
    for k, u in enumerate(test_set):
        print(f'Adaptive linear alg {j}')
        alg.reset_u(u)
        alg.construct_to_n(m)
        Vn_big = alg.Vn.orthonormalise()
        print(f'n: ', end='')
        for l, n in enumerate(range(2,min(Vn_big.n, m))):
            print(f'{n}, ', end='')
            Vn = Vn_big[:n]
            lin_est = rm.LinearWorstCaseEstimator(Vn=Vn, Wm=Wm)
            u_star, v_star = lin_est.best_estimator(Wm.A @ u)
            u_p_v = Vn @ (Vn.A @ u)

            stats[2, 0, j+len(Vns_linear), k, n] = lin_est.beta()
            stats[3, 0, j+len(Vns_linear), k, n] = lin_est.cond
            stats[0, 0, j+len(Vns_linear), k, n] = (u - u_star).norm()
            stats[1, 0, j+len(Vns_linear), k, n] = (u - u_p_v).norm()
        print('')

for j, alg in enumerate(algs_affine):
    for k, u in enumerate(test_set):
        print(f'Adaptive affine alg {j}')
        alg.reset_u(u)
        alg.construct_to_n(m)
        Vn_big = alg.Vn.orthonormalise()
        print(f'n: ', end='')
        for l, n in enumerate(range(2,min(Vn_big.n, m))):
            print(f'{n}, ', end='')
            Vn = Vn_big[:n]
            lin_est = rm.LinearWorstCaseEstimator(Vn=Vn, center=center, Wm=Wm)
            u_star, v_star = lin_est.best_estimator(Wm.A @ u)
            u_p_v = Vn @ (Vn.A @ (test_set - center))

            stats[2, 1, j+len(Vns_affine), k, n] = lin_est.beta()
            stats[3, 1, j+len(Vns_affine), k, n] = lin_est.cond
            stats[0, 1, j+len(Vns_affine), k, n] = (u - u_star).norm()
            stats[1, 1, j+len(Vns_affine), k, n] = (u - center - u_p_v).norm()
        print('')

dirResults = 'results/'
if not os.path.exists(dirResults):
    os.makedirs(dirResults)
np.save(f'results/02_data_based_greedy_w_affine_{Wm_type}_{m_or_div}', stats)
