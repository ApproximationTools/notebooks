import numpy as np
import scipy as sp

import sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as red

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(points):
        field = hil.Vector((a_bar + c * a_bar + c * p.reshape((2**field_div,2**field_div))).flatten(), space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns[:,i] = fem_solver.u
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)

field_div = 2
L2_coarse = hil.L2DyadicSq(field_div)

a_bar = 1.0
c = 0.9

side_n = 2**field_div
n_us = 40

np.random.seed(3)
points = 2*np.random.random((n_us, side_n*side_n)) - 1
us, fields = make_soln(points, L2_coarse, H1_fine, a_bar=a_bar, c=c, f=1.0)

#
# Make the two measurement spaces of regularly and randomly placed local averages...
# local_width is the width of the measurement squares in terms of FEM mesh squares
#
width_div = 1
local_width = 2**width_div
spacing_div = 4 

Wm_reg = red.make_local_avg_grid_basis(width_div, spacing_div, fem_div)
Wm_reg = Wm_reg.orthonormalise()

m = Wm_reg.n

Wm_rand = red.make_local_avg_random_basis(m=m, div=fem_div)
Wm_rand = Wm_rand.orthonormalise()

#
# Make the dictionary of random solutions for the greedy methods.
#
N = int(1e4)
dict_params = 2 * np.random.random((N, side_n * side_n)) - 1
dict_basis, dict_fields = make_soln(dict_params, L2_coarse, H1_fine, a_bar, c, f=1.0)

#
# Make sinusoid approx basis
#
Vn_sin = red.make_sin_basis(div=fem_div, N=8)

#
# Make random reduced basis
#
Vn_red_params = 2 * np.random.random((m, side_n * side_n)) - 1
Vn_red, Vn_red_fields = make_soln(Vn_red_params, L2_coarse, H1_fine, a_bar, c, f=1.0)

#
# Make greedily chosen reduced basis
#
g = red.GreedyApprox(dict_basis)
g.construct_to_n(m)

#
# Make PCA basis / estimator
#
Vn_PCA, sigma_sq, cent = red.pca_from_samples(dict_basis)

generic_Vns = [Vn_sin, Vn_red, g.Vn, Vn_PCA]
generic_Vns_labels = ['Sinusoid', 'Reduced', 'PlainGreedy', 'PCA']

#for Vn, label in zip(generic_Vns, generic_Vns_labels):
#    Vn.save('results/' + label + '_Basis')
#np.save('PCA_sigs', lam)

adapted_Vns = []
adapted_Vns_labels = ['MBOMP', 'MBPP']

for Wm, Wm_label in zip([Wm_reg, Wm_rand], ['Reg', 'Rand']):

    algs = [red.MeasBasedOMP(dict_basis, us[0], Wm, verbose=True), 
            red.MeasBasedPP(dict_basis, us[0], Wm, verbose=True)]
    
    adapted_Vns.append([])
    adapted_Vns_labels.append([])
    
    for alg, Vn_label in zip(algs, adapted_Vns_labels):
        adapted_Vns[-1].append([])
        for i, u in enumerate(us):
            if i > 0:
                alg.reset_u(u)
            alg.construct_to_n(m)
            #alg.Vn.save('results/' + Vn_label + '_' + Wm_label + '_{0}_Basis'.format(i))
            adapted_Vns[-1][-1].append(alg.Vn) 


stats = np.zeros([6, 2, 6, n_us, m]) # 6 stats, 2 Wms, 5 Vns (sin, red, greedy, omp, pp)

for i, (Wm, Wm_label) in enumerate(zip([Wm_reg, Wm_rand], ['Reg grid', 'Random'])):
    
    for j, g in enumerate(generic_Vns):
        Vn_big = g.orthonormalise()
        
        for l, n in enumerate(range(2,min(Vn_big.n, m))):
            
            Vn = Vn_big[:n]
            lin_est = red.LinearWorstCaseEstimator(Vn=Vn, Wm=Wm)

            stats[2, i, j, :, n] = lin_est.beta()

            for k, u in enumerate(us):
                u_p_v = Vn.project(u)
                u_star, v_star = lin_est.measure_and_reconstruct(u)
    
                stats[0, i, j, k, n] = (u - u_star).norm()
                stats[1, i, j, k, n] = (u - u_p_v).norm()
                stats[3, i, j, :, n] = cond
                stats[4, i, j, :, n] = (u_star - v_star).norm()
    
    for j_i, a in enumerate(adapted_Vns[i]):
        j = j_i + len(generic_Vns)
        for k, u in enumerate(us):

            Vn_big = a[k].orthonormalise()
            
            for l, n in enumerate(range(2,min(Vn_big.n, m))):
            
                Vn = Vn_big[:n]

                u_p_v = Vn.project(u)
                lin_est = red.LinearWorstCaseEstimator(Vn=Vn, Wm=Wm)
                u_star, v_star = lin_est.measure_and_reconstruct(u)

                stats[0, i, j, k, n] = (u - u_star).norm()
                stats[1, i, j, k, n] = (u - u_p_v).norm()
                stats[2, i, j, k, n] = lin_est.beta()
                stats[3, i, j, k, n] = cond
                stats[4, i, j, k, n] = (u_star - v_star).norm()

np.save('results/07_greedy_Vn_stats', stats)
