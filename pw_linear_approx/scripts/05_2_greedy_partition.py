import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

if not len(sys.argv) == 6:
    print(f'Usage: {sys.argv[0]}: field_div gamma Wm_spacing_div num_splits field_conv')
    sys.exit()

field_div = int(sys.argv[1])
gamma = float(sys.argv[2])
spacing_div = int(sys.argv[3])
num_splits = int(sys.argv[4])
field_conv = int(sys.argv[5])

print(f'RB training tests for field_div = {field_div}, gamma = {gamma}, ' 
      f'Wm_spacing_div = {spacing_div}, ' +
      f'num_splits = {num_splits}, field_conv = j^{-field_conv}')

# Spirals!
def spiral_cw(A):
    A = np.array(A)
    out = []
    while(A.size):
        out.append(A[0])        # take first row
        A = A[1:].T[::-1]       # cut off first row and rotate counterclockwise
    return np.concatenate(out)

def spiral_ccw(A):
    A = np.array(A)
    out = []
    while(A.size):
        out.append(A[0][::-1])    # first row reversed
        A = A[1:][::-1].T         # cut off first row and rotate clockwise
    return np.concatenate(out)

def base_spiral(nrow, ncol):
    return spiral_ccw(np.arange(nrow*ncol).reshape(nrow,ncol))[::-1]

def to_spiral(A):
    A = np.array(A)
    B = np.empty_like(A)
    B.flat[base_spiral(*A.shape)] = A.flat
    return B

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    loc_points = np.atleast_2d(points) 
    solns = hil.Basis(np.zeros((H1_fine.n, len(loc_points))), space=H1_fine)
    fields = []
     
    for i, p in enumerate(loc_points):
        field = hil.Vector(a_bar + c * p, space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
L2_coarse = hil.L2DyadicSq(field_div)
a_bar = 1.0
side_n = 2**field_div

index_spiral = to_spiral(np.arange(1,side_n**2+1).reshape((side_n, side_n)))
weight = ((1 / index_spiral) ** field_conv).flatten()

def diffusion_pde(points):
    solns, fields = make_soln(points, field_space=L2_coarse, fem_space=H1_fine, a_bar=a_bar, c=gamma)
    return solns

N_tr = 2000
N_te = 500

d = side_n * side_n
y_range = np.zeros((d,2))
y_range[:,0] = -1.0
y_range[:,1] = 1.0 
y_range = (y_range.T * weight).T

y_guesser = hil.DyadicGuesser(field_mean=a_bar, field_mult=gamma, fem_space=H1_fine, 
                              field_space=L2_coarse, y_bounds=y_range)

np.random.seed(1)
points_tr = np.random.random((N_tr, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_tr = diffusion_pde(points_tr)
points_te = np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_te = diffusion_pde(points_te)

U_tr = rm.ParamBasis(us_tr, points_tr)
U_te = us_te

width_div = 1
local_width = 2**width_div
#spacing_div = 5

Wm_reg = rm.make_local_avg_grid_basis(width_div, spacing_div, fem_div)
Wm_reg = Wm_reg.orthonormalise()
w_te = Wm_reg.A @ U_te
m = Wm_reg.n

# The steps roughly:
#  o From the training set generate the tree decomp
#  o For each test solution find the reconstruction based on param, best, and guess
#  o We want to compare the choice of estimators. Which has the best error? the best relationship?

stats = np.zeros((4, 2, num_splits+1, m-1, N_te))

lwc = rm.ParamDecompTreeWC(U_tr, diffusion_pde, y_range, est_n=Wm_reg.n, min_N=50, affine=True)
lin_est = lwc.lin_est
lin_est.Wm = Wm_reg

file_name = f'results/05_2_greedy_partition_{d}_{a_bar}_{gamma}_{spacing_div}_{num_splits}_{field_conv}'

print(f'Split 0')
print('Wm dim: ', end='')
for j in range(m-1):        
    print(f'{j+2} ', end='')
    lin_est_trunc = lin_est[:j+2]
    for k, u in enumerate(us_te):
        u_star = lin_est_trunc.best_estimate(w_te[:,k])
        stats[:,0,0,j,k] = (u_star - u).norm()
        stats[:,1,0,j,k] = 0
    np.save(file_name, stats)
print('') 

for i in range(1,num_splits+1):
    print(f'Split {i}... ', end='')
    lwc.split_leaf(directions='max', choose_big_leaf=True)
    print('done.')
    pw_est = lwc.oracle_estimator
    pw_est.Wm = Wm_reg

    if i % 2 == 0:
        print('Wm dim: ', end='')
        for j in range(m-1):
            print(f'{j+2} ', end='')
            pw_est_trunc = pw_est.truncate(slice(0,j+2))
            for k, u in enumerate(us_te):
                
                u_star_best, best_idx = pw_est_trunc.best_estimate(w_te[:,k], u, return_index=True)
                u_star_match, match_idx = pw_est_trunc.best_param_matching_estimate(w_te[:, k], 
                                                                                    points_te[k], return_index=True)
                u_star_approx, y_guess, u_guess, approx_idx = pw_est_trunc.best_guess_estimate(w_te[:, k], y_guesser, diffusion_pde, return_index=True)
                u_star_resid, y_guess, u_guess, resid_idx = pw_est_trunc.best_resid_estimate(w_te[:, k], y_guesser, diffusion_pde, return_index=True)
            
                stats[0,0,i,j,k] = (u_star_best - u).norm()
                stats[0,1,i,j,k] = best_idx
                stats[1,0,i,j,k] = (u_star_match - u).norm()
                stats[1,1,i,j,k] = match_idx
                stats[2,0,i,j,k] = (u_star_approx - u).norm()
                stats[2,1,i,j,k] = approx_idx
                stats[3,0,i,j,k] = (u_star_resid - u).norm()
                stats[3,1,i,j,k] = resid_idx
            
            np.save(file_name, stats)
        #pw_pickle_name = f'results/03_pw_est_{d}_{a_bar}_{spacing_div}_{num_splits}_{field_conv}_{i}'
        #pw_file = open(pw_pickle_name, 'wb')
        #pickle.dump(pw_est, pw_file)
        #pw_file.close()
        print('')
#tree_pickle_name = f'results/03_tree_{d}_{a_bar}_{spacing_div}_{num_splits}_{field_conv}_{i}'
#tree_file = open(tree_pickle_name, 'wb')
#pickle.dump(lwc, tree_file)
#tree_file.close()
