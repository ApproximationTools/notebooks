import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(points):
        field = hil.Vector(a_bar + c * p, space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
field_div = 2
L2_coarse = hil.L2DyadicSq(field_div)
a_bar = 0.1
c = 2.0
side_n = 2**field_div

def diffusion_pde(points):
    solns, fields = make_soln(points, field_space=L2_coarse, fem_space=H1_fine, a_bar=a_bar, c=c)
    return solns

N_tr = 5000
N_te = 500

d = side_n * side_n

y_range = np.zeros((d,2))
y_range[:,1] = 1

np.random.seed(1)
points_tr = np.random.random((N_tr, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_tr = diffusion_pde(points_tr)
points_te = np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_te = diffusion_pde(points_te)

U_tr = rm.ParamBasis(us_tr, points_tr)
U_te = us_te

width_div = 1
local_width = 2**width_div
spacing_div = 4

Wm_reg = rm.make_local_avg_grid_basis(width_div, spacing_div, fem_div)
Wm_reg = Wm_reg.orthonormalise()
w_te = Wm_reg.A @ U_te
m = Wm_reg.n

split_sets = tuple((i,) for i in range(3))

splits_single = tuple((i,) for i in range(8))
splits_two = ((0,1), (6,7), (0,15), (5,10))
splits_four = ((0,1,2,3), (5,6,9,10), (0,3,12,15))
splits = splits_single + splits_two + splits_four

lwc = rm.ParamDecompTreeWC(U_tr, diffusion_pde, y_range, est_n=Wm_reg.n, min_N=500)
lin_est = lwc.lin_est
lin_est.Wm = Wm_reg

stats = np.zeros([N_te, len(range(2,m,2))])

for i, n in enumerate(range(2,m,2)):
    trunc_lin_est = lin_est[:n]
    for k, u in enumerate(us_te):
        u_star = trunc_lin_est.best_estimate(w_te[:,k])
        stats[k, i] = (u - u_star).norm()

np.save(f'results/recon_error_{m}_full', stats)
for j, split in enumerate(splits):
    lwc.split_leaf(directions = split)
     
    stats = np.zeros([N_te, len(range(2,m,2))])
    pw_est = lwc.oracle_estimator
    pw_est.Wm = Wm_reg

    for i, n in enumerate(range(2,m,2)):
        trunc_pw_est = pw_est.truncate(slice(0,n))
        for k, u in enumerate(us_te):
            u_star = trunc_pw_est.best_estimate(w_te[:,k], points_te[k])
            stats[k, i] = (u - u_star).norm()
 
    for leaf in lwc.leaves:
        leaf.parent = None

    np.save(f'results/recon_error_{m}_{split}', stats)
