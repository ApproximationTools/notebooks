import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

if not len(sys.argv) == 6:
    print(f'Usage: {sys.argv[0]}: field_div gamma Wm_spacing_div num_splits field_conv')
    sys.exit()

field_div = int(sys.argv[1])
gamma = float(sys.argv[2])
spacing_div = int(sys.argv[3])
num_splits = int(sys.argv[4])
field_conv = int(sys.argv[5])

print(f'RB training tests for field_div = {field_div}, gamma = {gamma}, ' 
      f'Wm_spacing_div = {spacing_div}, ' +
      f'num_splits = {num_splits}, field_conv = j^{-field_conv}')

# Spirals!
def spiral_cw(A):
    A = np.array(A)
    out = []
    while(A.size):
        out.append(A[0])        # take first row
        A = A[1:].T[::-1]       # cut off first row and rotate counterclockwise
    return np.concatenate(out)

def spiral_ccw(A):
    A = np.array(A)
    out = []
    while(A.size):
        out.append(A[0][::-1])    # first row reversed
        A = A[1:][::-1].T         # cut off first row and rotate clockwise
    return np.concatenate(out)

def base_spiral(nrow, ncol):
    return spiral_ccw(np.arange(nrow*ncol).reshape(nrow,ncol))[::-1]

def to_spiral(A):
    A = np.array(A)
    B = np.empty_like(A)
    B.flat[base_spiral(*A.shape)] = A.flat
    return B

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    loc_points = np.atleast_2d(points) 
    solns = hil.Basis(np.zeros((H1_fine.n, len(loc_points))), space=H1_fine)
    fields = []
     
    for i, p in enumerate(loc_points):
        field = hil.Vector(a_bar + c * p, space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
L2_coarse = hil.L2DyadicSq(field_div)
a_bar = 1.0
side_n = 2**field_div

index_spiral = to_spiral(np.arange(1,side_n**2+1).reshape((side_n, side_n)))
weight = ((1 / index_spiral) ** field_conv).flatten()

def diffusion_pde(points):
    solns, fields = make_soln(points, field_space=L2_coarse, fem_space=H1_fine, a_bar=a_bar, c=gamma)
    return solns

N_tr = 4000
N_te = 1000

d = side_n * side_n
y_range = np.zeros((d,2))
y_range[:,0] = -1.0
y_range[:,1] = 1.0 
y_range = (y_range.T * weight).T

y_guesser = hil.DyadicGuesser(field_mean=a_bar, field_mult=gamma, fem_space=H1_fine, 
                              field_space=L2_coarse, y_bounds=y_range)

np.random.seed(1)
points_tr = np.random.random((N_tr, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_tr = diffusion_pde(points_tr)
points_te = np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_te = diffusion_pde(points_te)

U_tr = rm.ParamBasis(us_tr, points_tr)
U_te = us_te

width_div = 1
local_width = 2**width_div
#spacing_div = 5

Wm_reg = rm.make_local_avg_grid_basis(width_div, spacing_div, fem_div)
Wm_reg = Wm_reg.orthonormalise()
w_te = Wm_reg.A @ U_te
m = Wm_reg.n

# The steps roughly:
#  o From the training set generate the tree decomp
#  o For each test solution find the reconstruction based on param, best, and guess
#  o We want to compare the choice of estimators. Which has the best error? the best relationship?

stats = np.zeros((4, 2, num_splits+1, N_te))

inf_sups = []
epsilons = [] #np.zeros((num_splits+1,num_splits+1))

lwc = rm.ParamDecompTreeWC(U_tr, diffusion_pde, y_range, est_n=Wm_reg.n, min_N=50, affine=True, Wm=Wm_reg)
lin_est = lwc.lin_est
lin_est.Wm = Wm_reg

epsilons.append([lwc.residual,])
inf_sups.append([lwc.inf_sup,])

file_name = f'results/05_3_greedy_partition_sigma_admissible_{d}_{a_bar}_{gamma}_{spacing_div}_{num_splits}_{field_conv}'

print(f'Split 0')
print('(Leaf): dimension | ', end='')
print(f'(0): {lin_est.n}')
for k, u in enumerate(us_te):
    u_star = lin_est.best_estimate(w_te[:,k])
    stats[:,0,0,k] = (u_star - u).norm()
    stats[:,1,0,k] = 0
np.save(file_name, stats)

for i in range(1,num_splits+1):
    print(f'Split {i}... ')
    lwc.split_leaf(directions='max', choose_big_leaf=True)
    print('(Leaf): dimension | ', end='')
    for j, leaf in enumerate(lwc.leaves):
        print(f'({j}): {leaf.lin_est.n} ', end='')
    print('')
    pw_est = lwc.oracle_estimator
    pw_est.Wm = Wm_reg

    for k, u in enumerate(us_te):
        
        u_star_best, best_idx = pw_est.best_estimate(w_te[:,k], u, return_index=True)
        u_star_match, match_idx = pw_est.best_param_matching_estimate(w_te[:, k], points_te[k], return_index=True)
        u_star_approx, y_guess, u_guess, approx_idx = pw_est.best_guess_estimate(w_te[:, k], y_guesser, diffusion_pde, return_index=True)
        u_star_resid, y_guess, u_guess, resid_idx = pw_est.best_resid_estimate(w_te[:, k], y_guesser, diffusion_pde, return_index=True)
    
        stats[0,0,i,k] = (u_star_best - u).norm()
        stats[0,1,i,k] = best_idx
        stats[1,0,i,k] = (u_star_match - u).norm()
        stats[1,1,i,k] = match_idx
        stats[2,0,i,k] = (u_star_approx - u).norm()
        stats[2,1,i,k] = approx_idx
        stats[3,0,i,k] = (u_star_resid - u).norm()
        stats[3,1,i,k] = resid_idx
    
        if k % 10 == 0:
            np.save(file_name, stats)
    
    pw_pickle_name = f'results/05_3_greedy_partition_sigma_admissible_{d}_{a_bar}_{spacing_div}_{num_splits}_{field_conv}_{i}'
    pw_file = open(pw_pickle_name, 'wb')
    pickle.dump(pw_est, pw_file)
    pw_file.close()

    epsilons.append([leaf.residual for leaf in lwc.leaves])
    inf_sups.append([leaf.inf_sup for leaf in lwc.leaves])

resids_file_name = f'results/05_3_greedy_partition_sigma_admissible_residuals_{d}_{a_bar}_{gamma}_{spacing_div}_{num_splits}_{field_conv}'
inf_sups_file_name = f'results/05_3_greedy_partition_sigma_admissible_inf_sups_{d}_{a_bar}_{gamma}_{spacing_div}_{num_splits}_{field_conv}'
np.save(resids_file_name, np.array(epsilons))
np.save(inf_sups_file_name, np.array(inf_sups))

