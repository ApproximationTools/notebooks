import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

if not len(sys.argv) == 7:
    print(f'Usage: {sys.argv[0]}: m gamma N_tr N_te Wm_sym Vn_sym')
    sys.exit()

m = int(sys.argv[1])
gamma = float(sys.argv[2])
N_tr = int(sys.argv[3])
N_te = int(sys.argv[4])
Wm_sym = bool(int(sys.argv[5]))
Vn_sym = bool(int(sys.argv[6]))

print(f'Two space pw-linear RB with m = {m}, N_tr = {N_tr}, N_te = {N_te}, Wm_sym = {Wm_sym}, Vn_sym = {Vn_sym}')

def make_soln(points, field_basis, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(np.atleast_2d(points)):
        field = c * (field_basis @ p) + a_bar
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
field_div = 2
L2_coarse = hil.L2DyadicSq(field_div)
a_bar = 1.0
c = gamma 
side_n = 2**field_div
d = 4
y_range = np.zeros((d,2))
y_range[:,0] = -1.0
y_range[:,1] = 1.0

field_basis_1 = hil.Basis(values=np.array([np.pad(e.reshape((2,2)), ((0,2),(0,2)), mode='edge').flatten() for e in np.eye(4)]).T, space=L2_coarse)
field_basis_2 = hil.Basis(values=np.array([np.pad(e.reshape((2,2)), ((2,0),(2,0)), mode='edge').flatten() for e in np.eye(4)]).T, space=L2_coarse)

def diffusion_pde_1(points):
    solns, fields = make_soln(points, field_basis=field_basis_1, fem_space=H1_fine, a_bar=a_bar, c=c)
    return solns
def diffusion_pde_2(points):
    solns, fields = make_soln(points, field_basis=field_basis_2, fem_space=H1_fine, a_bar=a_bar, c=c)
    return solns

np.random.seed(2)
points_trs = []; points_tes = []
us_trs = []; us_tes = []

if not Vn_sym:
    for pde in [diffusion_pde_1, diffusion_pde_2]:
        points_trs.append(np.random.random((N_tr, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0])
        us_trs.append(pde(points_trs[-1]))
        points_tes.append(np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0])
        us_tes.append(pde(points_tes[-1]))
else:
    points_trs.append(np.random.random((N_tr, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0])
    us_trs.append(diffusion_pde_1(points_trs[-1]))
    points_trs.append(points_trs[-1][:,::-1])
    us_trs.append(diffusion_pde_2(points_trs[-1]))
    points_tes.append(np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0])
    us_tes.append(diffusion_pde_1(points_tes[-1]))
    points_tes.append(points_tes[-1][:,::-1])
    us_tes.append(diffusion_pde_2(points_tes[-1]))

us_trs.append(copy.deepcopy(us_trs[0]).append(copy.deepcopy(us_trs[1])))

y_guessers = [hil.DyadicGuesser(field_mean=a_bar, field_mult=c, fem_space=H1_fine, field_space=L2_coarse, field_basis=field_basis, y_bounds=y_range) for field_basis in [field_basis_1, field_basis_2]]

# local_width is the width of the measurement squares in terms of FEM mesh squares
width_div = 2
spacing_div = 5
L2_loc = hil.L2DyadicSq(fem_div)

#Wm_reg, Wm_reg_L2, Wloc_reg = rm.make_local_avg_grid_basis(width_div, spacing_div, fem_div, return_map=True)

seed = 14
np.random.seed(seed)
if not Wm_sym:
    Wm_rand, Wm_rand_L2, Wloc_rand = rm.make_local_avg_random_basis(m, fem_div, return_map=True)
    Wm = Wm_rand.orthonormalise()
else:
    Wm_rand_sym, Wm_rand_sym_L2, Wloc_rand_sym = rm.make_local_avg_random_basis(m//2, fem_div, return_map=True)
    Wm_rand_sym_rev = copy.deepcopy(Wm_rand_sym)
    for i, col in enumerate(Wm_rand_sym_rev.values.T):
        new_col = col.reshape((Wm_rand_sym_rev.space.side_len, Wm_rand_sym_rev.space.side_len))[::-1, ::-1].flatten()
        Wm_rand_sym_rev.values[:, i] = new_col
    Wloc_rand_sym = Wloc_rand_sym[::-1,::-1] + Wloc_rand_sym
    Wm_rand_sym = Wm_rand_sym.append(Wm_rand_sym_rev)
    Wm = Wm_rand_sym.orthonormalise()

us_trs_mean = [us @ (np.ones(us.n) / us.n) for us in us_trs]
# The PDEs both have the same mid point
u_mid = diffusion_pde_1(np.zeros((1,4)))[0]
us_trs_mf = [us - u_mid for us in us_trs]

greedys = [rm.GreedyApprox(us, verbose=True) for us in us_trs_mf]
Vns = [greedy.construct_to_n(Wm.n+1) for greedy in greedys]

# Set n for each sub manifold based on the poor-man's algorithm
poor_man_sels = np.zeros((3), dtype=int)
inf_sups = np.zeros((3,Wm.n))
epsilons = np.zeros((3,Wm.n))

for j, (greedy, Vn) in enumerate(zip(greedys, Vns)):
    epsilons[j, :] = greedy.residuals[1:]
    
    inf_sups[j, 0] = np.linalg.norm(Wm.A @ Vn[0]/Vn[0].norm())

    for i in range(2,Wm.n+1):
        lin_est = rm.LinearWorstCaseEstimator(Vn[:i].orthonormalise(), Wm=Wm)
        inf_sups[j, i-1] = lin_est.beta()
    
    poor_man_sels[j] = np.argmin(epsilons[j,:] / inf_sups[j, :])
    print(f'Vn {j}, poor mans n={poor_man_sels[j]+1}')

# This code generates the error measurements for all n from 1 to n
full_stats = np.zeros((len(Vns), len(us_tes), Wm.n, N_te, 2))
for j, (Vn, Vn_mean) in enumerate(zip(Vns, us_trs_mean)):
    full_CG = Wm.A @ Vn
    n_max = (np.linalg.eigh(full_CG.T @ full_CG)[0] > 1e-14).sum()
    for k, us_te in enumerate(us_tes):
        ws_te = Wm.A @ us_te
        for n_loc in range(n_max):
            est = rm.LinearWorstCaseEstimator(Vn[:n_loc+1].orthonormalise(), center=Vn_mean, Wm=Wm)
            u_stars = est.best_estimate(ws_te)
            full_stats[j, k, n_loc, :, 0] = (us_te - u_stars).norms()
            for l, (u_te, w_te) in enumerate(zip(us_te, ws_te.T)):
                if j < 2:
                    y_guess_star, residual_star = y_guessers[j].nearest_params_resid(u_stars[l])
                    full_stats[j, k, n_loc, l, 1] = residual_star

file_name_base = f'results/05_1_two_space_{Wm.n}_{gamma}_{N_tr}_{N_te}_affine'

if Wm_sym:
    file_name_base += '_Wm_sym'
if Vn_sym:
    file_name_base += '_Vn_sym'

np.save(file_name_base + '_results', full_stats)
np.save(file_name_base + '_inf_sup', inf_sups)
np.save(file_name_base + '_epsilons', epsilons)
np.save(file_name_base + '_poor_mans_sel', poor_man_sels)
