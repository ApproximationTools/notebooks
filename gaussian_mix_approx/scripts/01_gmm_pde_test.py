import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(points):
        field = hil.Vector(a_bar + c * p, space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
field_div = 2
L2_coarse = hil.L2DyadicSq(field_div)
a_bar = 0.1
c = 2.0
side_n = 2**field_div

def diffusion_pde(points):
    solns, fields = make_soln(points, field_space=L2_coarse, fem_space=H1_fine, a_bar=a_bar, c=c)
    return solns

N_tr = 10000
N_te = 1000

d = side_n * side_n

y_range = np.zeros((d,2))
y_range[:,1] = 1

np.random.seed(1)
points_tr = np.random.random((N_tr, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_tr = diffusion_pde(points_tr)
points_te = np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_te = diffusion_pde(points_te)

U_tr = rm.ParamBasis(us_tr, points_tr)
U_te = us_te

lpca = rm.ParamDecompTree(U_tr, diffusion_pde, y_range, min_N=50)

mixture_ests = [copy.deepcopy(lpca.mixture_estimator)]
pickle_file = open('results/mixture_est_1.p', 'wb')
pickle.dump(lpca.mixture_estimator, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)

num_of_splittings = 7
for i in range(num_of_splittings):
    print('\nTree at level ' + str(i+1))
    print(anytree.RenderTree(lpca).by_attr())
    lpca.split_leaf()
    mixture_ests.append(copy.deepcopy(lpca.mixture_estimator))    
    pickle_file = open(f'results/mixture_est_{i+2}.p', 'wb')
    pickle.dump(lpca.mixture_estimator, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)

print('\nTree at level ' + str(i+1))
print(anytree.RenderTree(lpca).by_attr())

#
# Make the two measurement spaces of regularly and randomly placed local averages...
# local_width is the width of the measurement squares in terms of FEM mesh squares
#
width_div = 1
local_width = 2**width_div

for spacing_div in [3,4,5]:

    Wm_reg = rm.make_local_avg_grid_basis(width_div, spacing_div, fem_div)
    Wm_reg = Wm_reg.orthonormalise()

    m = Wm_reg.n

    Wm_rand = rm.make_local_avg_random_basis(m=m, div=fem_div)
    Wm_rand = Wm_rand.orthonormalise()
    
    stats = np.zeros([2, len(mixture_ests), N_te, len(range(2,2*m,2))])

    Wm = Wm_reg
    for mixture_est in mixture_ests:
        mixture_est.Wm = Wm
    w_te = Wm.A @ U_te

    for i, n in enumerate(range(2,2*m,2)):
        for j, mixture_est in enumerate(mixture_ests):
            trunc_est = mixture_est.truncate(slice(0,n))
            for k, u in enumerate(U_te):
                
                u_star = trunc_est.best_estimate(w_te[:,k])
                stats[0, j, k, i] = (u - u_star).norm()
 
    Wm = Wm_rand
    for mixture_est in mixture_ests:
        mixture_est.Wm = Wm
    w_te = Wm.A @ U_te

    for i, n in enumerate(range(2,2*m,2)):
        for j, mixture_est in enumerate(mixture_ests):
            trunc_est = mixture_est.truncate(slice(0,n))
            for k, u in enumerate(U_te):
                
                u_star = trunc_est.best_estimate(w_te[:,k])
                stats[1, j, k, i] = (u - u_star).norm()

    np.save(f'results/recon_error_{m}', stats)
