import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

field_div = int(sys.argv[1])
a_bar = float(sys.argv[2])

print(f'RB training tests for field_div {field_div} and a_bar {a_bar}')

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(points):
        field = hil.Vector(a_bar + c * p, space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
L2_coarse = hil.L2DyadicSq(field_div)
c = 2.0
side_n = 2**field_div

def diffusion_pde(points):
    solns, fields = make_soln(points, field_space=L2_coarse, fem_space=H1_fine, a_bar=a_bar, c=c)
    return solns

N_tr = 10000
N_te = 10000

d = side_n * side_n

y_range = np.zeros((d,2))
y_range[:,1] = 1

np.random.seed(1)
points_te = np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_te = diffusion_pde(points_te)

import copy

if field_div < 2:
    n = 20
else:
    n = 50
Ns = [n, 100, 500, 1000, 5000, 10000]
    
points_tr = np.random.random((Ns[0], d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_tr = diffusion_pde(points_tr)
Vns = [copy.deepcopy(us_tr[:Ns[0]])]

for N in Ns[1:]:
    print(f'Constructing greedy with dict size {N}...')
    points_tr = np.random.random((N, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
    us_tr = diffusion_pde(points_tr)
    greedy = rm.GreedyApprox(us_tr)
    Vn = copy.deepcopy(greedy.construct_to_n(n))
    Vns.append(Vn)
    del greedy

avg_errors = np.zeros((len(Vns), max([Vn.n for Vn in Vns])))
max_errors = np.zeros((len(Vns), max([Vn.n for Vn in Vns])))
for i, Vn in enumerate(Vns):
    print(f'Calculating error for Vn constructed from dict of size {Ns[i]}')
    
    # First we project to the one vector, which doesn't have a routine
    v = Vn[0] / Vn[0].norm()
    us_te_proj = (us_te.A @ v)
    errors = np.array([(us_te[i] - us_te_proj[i]*v).norm() for i in range(us_te.n)])
    max_errors[i, 0] = errors.max()
    avg_errors[i, 0] = errors.mean()
    
    for n_trunc in range(1, Vn.n):
        Vn_trunc_ortho = Vn[:n_trunc+1].orthonormalise()
        us_te_proj = Vn_trunc_ortho @ (Vn_trunc_ortho.A @ us_te)
        errors = (us_te - us_te_proj).norms()

        max_errors[i, n_trunc] = errors.max()
        avg_errors[i, n_trunc] = errors.mean()

np.save(f'results/avg_errors_{d}_{a_bar}', avg_errors)
np.save(f'results/max_errors_{d}_{a_bar}', max_errors)
