import numpy as np
import scipy as sp

import anytree

import copy, pickle, sys
sys.path.append("../../../")
import pyhilbert as hil
import pyredmod as rm

if not len(sys.argv) == 6:
    print(f'Usage: {sys.argv[0]}: field_div a_bar n_max num_runs field_conv')
    sys.exit()

field_div = int(sys.argv[1])
a_bar = float(sys.argv[2])
n_max = int(sys.argv[3])
num_runs = int(sys.argv[4])
field_conv = int(sys.argv[5])

print(f'RB training tests for field_div = {field_div}, a_bar = {a_bar}, n_max = {n_max}, num_runs = {num_runs}, field_conv = j^{-field_conv}')

# Spirals!
def spiral_cw(A):
    A = np.array(A)
    out = []
    while(A.size):
        out.append(A[0])        # take first row
        A = A[1:].T[::-1]       # cut off first row and rotate counterclockwise
    return np.concatenate(out)

def spiral_ccw(A):
    A = np.array(A)
    out = []
    while(A.size):
        out.append(A[0][::-1])    # first row reversed
        A = A[1:][::-1].T         # cut off first row and rotate clockwise
    return np.concatenate(out)

def base_spiral(nrow, ncol):
    return spiral_ccw(np.arange(nrow*ncol).reshape(nrow,ncol))[::-1]

def to_spiral(A):
    A = np.array(A)
    B = np.empty_like(A)
    B.flat[base_spiral(*A.shape)] = A.flat
    return B

def make_soln(points, field_space, fem_space, a_bar=1.0, c=0.5, f=1.0, verbose=False):
    
    solns = hil.Basis(np.zeros((H1_fine.n, len(points))), space=H1_fine)
    fields = []
    
    for i, p in enumerate(points):
        field = hil.Vector(a_bar + c * p, space=field_space)
        fields.append(field)
        # Then the fem solver (there a faster way to do this all at once? This will be huge...
        fem_solver = hil.DyadicFEMSolver(fem_space = fem_space, rand_field = field, f = 1)
        fem_solver.solve()
        solns._values[:,i] = fem_solver.u.values
        
    return solns, fields

fem_div = 7
H1_fine = hil.H1DyadicSq(fem_div)
L2_coarse = hil.L2DyadicSq(field_div)
c = 1.0
side_n = 2**field_div

index_spiral = to_spiral(np.arange(1,side_n**2+1).reshape((side_n, side_n)))
weight = ((1 / index_spiral) ** field_conv).flatten()

def diffusion_pde(points):
    points *= weight
    solns, fields = make_soln(points, field_space=L2_coarse, fem_space=H1_fine, a_bar=a_bar, c=c)
    return solns

alphas = [1.0, 1.25, 1.5, 1.75, 2.0]

N_te = 100

d = side_n * side_n

y_range = np.zeros((d,2))
y_range[:,0] = -1
y_range[:,1] = 1

np.random.seed(1)
points_te = np.random.random((N_te, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
us_te = diffusion_pde(points_te)

avg_errors = np.zeros((len(alphas), num_runs, n_max))
max_errors = np.zeros((len(alphas), num_runs, n_max))

ns = np.arange(2, n_max+1)

for i, alpha in enumerate(alphas):
    print(f'Running alpha = {alpha} with field_div = {field_div}, a_bar = {a_bar}, n_max = {n_max}, num_runs = {num_runs}, field_conv = j^{-field_conv}')
    Ns = np.round(ns**alpha).astype(int)
    for j in range(num_runs):
        print(f'Run number {j}')
        for k, n in enumerate(ns):
            N = Ns[k]
            
            if k == 0:
                points_tr = np.random.random((N, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
                us_tr = diffusion_pde(points_tr)
                greedy = rm.GreedyApprox(us_tr)
            else:
                N_diff = (Ns[k] - Ns[k-1])
                points_addition = np.random.random((N_diff, d)) * (y_range[:, 1] - y_range[:, 0]) + y_range[:, 0]
                us_addition = diffusion_pde(points_addition)
                us_tr.append(us_addition)
                greedy.dictionary = us_tr
            
            Vn = greedy.construct_to_n(n)
            Vn_ortho = greedy.Vn.orthonormalise()
            Vn_ortho_old = Vn_ortho
            us_te_proj = Vn_ortho @ (Vn_ortho.A @ us_te)
            errors = (us_te - us_te_proj).norms()

            max_errors[i, j, n-1] = errors.max()
            avg_errors[i, j, n-1] = errors.mean()

        np.save(f'results/02_avg_errors_embedded_{d}_{a_bar}_{n_max}_{num_runs}_{field_conv}', avg_errors)
        np.save(f'results/02_max_errors_embedded_{d}_{a_bar}_{n_max}_{num_runs}_{field_conv}', max_errors)
